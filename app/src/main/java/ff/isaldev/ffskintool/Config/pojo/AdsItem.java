package ff.isaldev.ffskintool.Config.pojo;

import com.google.gson.annotations.SerializedName;

public class AdsItem{

	@SerializedName("main_ads_rewards")
	private String mainAdsRewards;

	@SerializedName("interval_intertitial")
	private int intervalIntertitial;

	@SerializedName("initialize_sdk_backup_ads")
	private String initializeSdkBackupAds;

	@SerializedName("initialize_sdk")
	private String initializeSdk;

	@SerializedName("switch_open_ads")
	private String switchOpenAds;

	@SerializedName("backup_ads_banner")
	private String backupAdsBanner;

	@SerializedName("interval_rewards")
	private int intervalRewards;

	@SerializedName("backup_ads_rewards")
	private String backupAdsRewards;

	@SerializedName("backup_ads_natives")
	private String backupAdsNatives;

	@SerializedName("backup_ads_intertitial")
	private String backupAdsIntertitial;

	@SerializedName("high_paying_keyword_2")
	private String highPayingKeyword2;

	@SerializedName("high_paying_keyword_3")
	private String highPayingKeyword3;

	@SerializedName("open_ads_admob")
	private String openAdsAdmob;

	@SerializedName("high_paying_keyword_4")
	private String highPayingKeyword4;

	@SerializedName("main_ads_banner")
	private String mainAdsBanner;

	@SerializedName("high_paying_keyword_5")
	private String highPayingKeyword5;

	@SerializedName("high_paying_keyword_1")
	private String highPayingKeyword1;

	@SerializedName("main_ads_intertitial")
	private String mainAdsIntertitial;

	@SerializedName("status_app")
	private String statusApp;

	@SerializedName("select_backup_ads")
	private String selectBackupAds;

	@SerializedName("main_ads_natives")
	private String mainAdsNatives;

	@SerializedName("select_main_ads")
	private String selectMainAds;

	@SerializedName("link_redirect")
	private String linkRedirect;

	public String getOnesignal_apikey() {
		return onesignal_apikey;
	}

	@SerializedName("onesignal_apikey")
	private String onesignal_apikey;

	public String getMainAdsRewards(){
		return mainAdsRewards;
	}

	public int getIntervalIntertitial(){
		return intervalIntertitial;
	}

	public String getInitializeSdkBackupAds(){
		return initializeSdkBackupAds;
	}

	public String getInitializeSdk(){
		return initializeSdk;
	}

	public String getSwitchOpenAds(){
		return switchOpenAds;
	}

	public String getBackupAdsBanner(){
		return backupAdsBanner;
	}

	public int getIntervalRewards(){
		return intervalRewards;
	}

	public String getBackupAdsRewards(){
		return backupAdsRewards;
	}

	public String getBackupAdsNatives(){
		return backupAdsNatives;
	}

	public String getBackupAdsIntertitial(){
		return backupAdsIntertitial;
	}

	public String getHighPayingKeyword2(){
		return highPayingKeyword2;
	}

	public String getHighPayingKeyword3(){
		return highPayingKeyword3;
	}

	public String getOpenAdsAdmob(){
		return openAdsAdmob;
	}

	public String getHighPayingKeyword4(){
		return highPayingKeyword4;
	}

	public String getMainAdsBanner(){
		return mainAdsBanner;
	}

	public String getHighPayingKeyword5(){
		return highPayingKeyword5;
	}

	public String getHighPayingKeyword1(){
		return highPayingKeyword1;
	}

	public String getMainAdsIntertitial(){
		return mainAdsIntertitial;
	}

	public String getStatusApp(){
		return statusApp;
	}

	public String getSelectBackupAds(){
		return selectBackupAds;
	}

	public String getMainAdsNatives(){
		return mainAdsNatives;
	}

	public String getSelectMainAds(){
		return selectMainAds;
	}

	public String getLinkRedirect(){
		return linkRedirect;
	}
}