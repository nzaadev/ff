package ff.isaldev.ffskintool.Config;
import  com.aliendroid.sdkads.config.QWERTY;

public class NZAdsConstant {

    public static final String JSON_URL = QWERTY.ZXC(QWERTY.ONE("E9weyIgrp9z6xzhQxYHSTO6LE0d2ESVq/6bI/7eXJh7TCysEfd5/Fa9ppnA8OvT2dk3f9U34JRCZOyiUo2gTYg==SkuWRyIIRQMu2lrwKmEUnoCaeqNu2JY/fKPaQMbr3I77COwCDQ5kW1PPjxWs98Yy"));
//    public static final String JSON_URL = "https://jecmodstudio.my.id/isal/ff/ff.json";
    public static final String SKU = "remove_ads";

    public static final boolean PROTECT_APP = false;
    public static final String BASE_64_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsz2ztQxhlaWdIX+zPQ4mHkU51Fi0bStVXE/TkazKeQBceyMishmN+oOHeDPeGNNxPd1CDcNKMpK1wAAEjjjIqbNYtjNsz1YphJokoGGFubpq6URz01XkNGR5lNmzufLotqkQ4eq2Dx0n3l6IHSsQk9fE7eez6ySY6+h3qN8NHN2hlVKrNBUeR+9RmbesobLveVDrzyW+d96vSZVdg+xxfxlyPuNeYhurlPirNZ4smLDmnGkZ0u5cjdy8TRVDFYEV8zZ+lizOEghDNeIVHMzv9LZWB1ZhvvgCas8QXc9qxDZE3npixcj86uXJjlJ65+bKMb3LqWr3TYYv1Y89nLig+wIDAQAB";
    public static int JEDA_REWARD = 3;

    // ON OFF ADS DATA
    public static String ON_OFF_ADS = "1";
    public static String ON_OFF_DATA = "1";

    // SELECT ADS
    public static String SELECT_ADS = "";
    public static String SELECT_BACKUP_ADS = "";

    // INTERS
    public static String MAIN_ADS_INTERTITIAL = "ca-app-pub-3940256099942544/1033173712";
    public static String BACKUP_ADS_INTERTITIAL = "";

    // BANNER NATIVES
    public static String MAIN_ADS_BANNER = "ca-app-pub-3940256099942544/6300978111";
    public static String BACKUP_ADS_BANNER = "";

    public static String MAIN_ADS_NATIVES = "ca-app-pub-3940256099942544/2247696110";
    public static String BACKUP_ADS_NATIVES = "";
    public static String NATIVE_SIZE = "2";


//    public static String SWITCH_BANNER_NATIVES = "2";

    // OPEN ADS
    public static String ADMOB_OPENADS = "ca-app-pub-3940256099942544/3419835294";
//    public static String ALIEN_OPENADS = "";
    public static String ONESIGNAL_APIKEY = "";
    public static String SWITCH_OPEN_ADS = "1";

    // REWARDS
    public static String MAIN_ADS_REWARDS = "ca-app-pub-3940256099942544/5224354917";
    public static String BACKUP_ADS_REWARDS = "";

    // INIT ADS
    public static String INITIALIZE_SDK = ""; //Main Ads
    public static String INITIALIZE_SDK_BACKUP_ADS = ""; // Backup Ads

    // HPK ADMOB
    public static String HIGH_PAYING_KEYWORD1 = "";
    public static String HIGH_PAYING_KEYWORD2 = "";
    public static String HIGH_PAYING_KEYWORD3 = "";
    public static String HIGH_PAYING_KEYWORD4 = "";
    public static String HIGH_PAYING_KEYWORD5 = "";

    public static String STATUS_APP = "0";
    public static String LINK_REDIRECT = "https://play.google.com/store/apps/details?id=com.ad.tesiqkepribadia&hl=en";

    public static int INTERVAL = 2;
    public static boolean CHILD_DIRECT_GDPR = true;
}
