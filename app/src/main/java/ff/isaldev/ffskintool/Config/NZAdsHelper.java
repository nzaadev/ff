package ff.isaldev.ffskintool.Config;

import static ff.isaldev.ffskintool.Config.NZAdsConstant.ADMOB_OPENADS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.BACKUP_ADS_BANNER;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.BACKUP_ADS_INTERTITIAL;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.BACKUP_ADS_NATIVES;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.BACKUP_ADS_REWARDS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.CHILD_DIRECT_GDPR;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.HIGH_PAYING_KEYWORD1;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.HIGH_PAYING_KEYWORD2;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.HIGH_PAYING_KEYWORD3;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.HIGH_PAYING_KEYWORD4;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.HIGH_PAYING_KEYWORD5;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.INITIALIZE_SDK;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.INITIALIZE_SDK_BACKUP_ADS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.INTERVAL;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.MAIN_ADS_BANNER;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.MAIN_ADS_INTERTITIAL;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.MAIN_ADS_NATIVES;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.MAIN_ADS_REWARDS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.NATIVE_SIZE;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.SELECT_ADS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.SELECT_BACKUP_ADS;
import static ff.isaldev.ffskintool.Config.NZAdsConstant.SWITCH_OPEN_ADS;

import android.app.Activity;
import android.widget.RelativeLayout;

import com.aliendroid.alienads.AlienGDPR;
import com.aliendroid.alienads.AlienOpenAds;
import com.aliendroid.alienads.AliendroidBanner;
import com.aliendroid.alienads.AliendroidInitialize;
import com.aliendroid.alienads.AliendroidIntertitial;
import com.aliendroid.alienads.AliendroidNative;
import com.aliendroid.alienads.AliendroidReward;
import com.aliendroid.alienads.interfaces.rewards.load.OnLoadRewardsApplovinDiscovery;
import com.aliendroid.alienads.interfaces.rewards.load.OnLoadRewardsApplovinMax;
import com.aliendroid.alienads.interfaces.rewards.show.OnShowRewardsAdmob;


public class NZAdsHelper  {


    public static void initialize(Activity context) {
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidInitialize.SelectAdsAdmob(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "GOOGLE-ADS":
                AliendroidInitialize.SelectAdsGoogleAds(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                AliendroidInitialize.SelectAdsApplovinDis(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidInitialize.SelectAdsApplovinMax(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "MOPUB":
                AliendroidInitialize.SelectAdsMopub(context, SELECT_BACKUP_ADS, INITIALIZE_SDK, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "IRON":
                AliendroidInitialize.SelectAdsIron(context, SELECT_BACKUP_ADS, INITIALIZE_SDK, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "STARTAPP":
                AliendroidInitialize.SelectAdsStartApp(context, SELECT_BACKUP_ADS, INITIALIZE_SDK, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "UNITY":
                AliendroidInitialize.SelectAdsUnity(context, SELECT_BACKUP_ADS, INITIALIZE_SDK, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "FACEBOOK":
                AliendroidInitialize.SelectAdsFAN(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "ALIEN-M":
                AliendroidInitialize.SelectAdsAlienMediation(context, SELECT_BACKUP_ADS,INITIALIZE_SDK, INITIALIZE_SDK_BACKUP_ADS);
                break;
            case "ALIEN-V":
                AliendroidInitialize.SelectAdsAlienView(context, SELECT_BACKUP_ADS, INITIALIZE_SDK_BACKUP_ADS);
                break;
        }
    }

    
    public static void adsGdpr(Activity context) {
        AlienGDPR.loadGdpr(context, SELECT_ADS, CHILD_DIRECT_GDPR);
    }


    public static void loadOpenAds(Activity activity) {
        if(SELECT_ADS.equals("ADMOB")) {
            AlienOpenAds.LoadOpenAds(NZAdsConstant.ADMOB_OPENADS, true);
            AlienOpenAds.AppOpenAdManager.loadAd(activity);
        }
    }

    public static void showOpenAds(Activity activity) {
        if(SELECT_ADS.equals("ADMOB")) {
            AlienOpenAds.AppOpenAdManager.showAdIfAvailable(activity);
        }
    }

    
    public static void banner(Activity context, RelativeLayout view) {
        switch (SELECT_ADS) {
            case "ADMOB":
                    AliendroidBanner.SmallBannerAdmob(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER, HIGH_PAYING_KEYWORD1,
                            HIGH_PAYING_KEYWORD2, HIGH_PAYING_KEYWORD3, HIGH_PAYING_KEYWORD4, HIGH_PAYING_KEYWORD5);
                break;
            case "APPLOVIN-M":
                    AliendroidBanner.SmallBannerApplovinMax(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "APPLOVIN-D":
                AliendroidBanner.SmallBannerApplovinDis(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "MOPUB":
                AliendroidBanner.SmallBannerMopub(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "STARTAPP":
                AliendroidBanner.SmallBannerStartApp(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "IRON":
                AliendroidBanner.SmallBannerIron(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "FACEBOOK":
                AliendroidBanner.SmallBannerFAN(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "UNITY":
                AliendroidBanner.SmallBannerUnity(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "GOOGLE-ADS":
                AliendroidBanner.SmallBannerGoogleAds(context, view, SELECT_BACKUP_ADS, MAIN_ADS_BANNER, BACKUP_ADS_BANNER);
                break;
            case "ALIEN-M":
                AliendroidBanner.SmallBannerAlienMediation(context,view, SELECT_BACKUP_ADS,MAIN_ADS_BANNER,BACKUP_ADS_BANNER);
               break;
            case "ALIEN-V":
                AliendroidBanner.SmallBannerAlienView(context,view, SELECT_BACKUP_ADS,MAIN_ADS_BANNER,BACKUP_ADS_BANNER);
                break;
        }
    }

    
    public static void natives(Activity context, RelativeLayout view) {
        switch (SELECT_ADS) {
            case "ADMOB":
                if(NATIVE_SIZE.equals("1")) {
                    AliendroidNative.SmallNativeAdmob(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES, HIGH_PAYING_KEYWORD1, HIGH_PAYING_KEYWORD2, HIGH_PAYING_KEYWORD3, HIGH_PAYING_KEYWORD4, HIGH_PAYING_KEYWORD5);
                } else {
                    AliendroidNative.MediumNativeAdmob(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES, HIGH_PAYING_KEYWORD1, HIGH_PAYING_KEYWORD2, HIGH_PAYING_KEYWORD3, HIGH_PAYING_KEYWORD4, HIGH_PAYING_KEYWORD5);
                }
                break;
            case "APPLOVIN-M":
                if(NATIVE_SIZE.equals("1")) {
                    AliendroidNative.SmallNativeMax(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                } else {
                    AliendroidNative.MediumNativeMax(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                }
                break;

            case "STARTAPP":
                if(NATIVE_SIZE.equals("1")) {
                    AliendroidNative.SmallNativeStartApp(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                } else {
                    AliendroidNative.MediumNativeStartApp(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                }
                break;
            case "FACEBOOK":
            case "FAN":
                if(NATIVE_SIZE.equals("1")) {
                    AliendroidNative.SmallNativeFan(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                } else {
                    AliendroidNative.MediumNativeFan(context, view, SELECT_BACKUP_ADS, MAIN_ADS_NATIVES, BACKUP_ADS_NATIVES);
                }
                break;
        }
    }

    
    public static void loadInterstitial(Activity context) {
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidIntertitial.LoadIntertitialAdmob(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, HIGH_PAYING_KEYWORD1,
                        HIGH_PAYING_KEYWORD2, HIGH_PAYING_KEYWORD3, HIGH_PAYING_KEYWORD4, HIGH_PAYING_KEYWORD5);
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidIntertitial.LoadIntertitialApplovinMax(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                AliendroidIntertitial.LoadIntertitialApplovinDis(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "MOPUB":
                AliendroidIntertitial.LoadIntertitialMopub(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "STARTAPP":
                AliendroidIntertitial.LoadIntertitialStartApp(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "IRON":
                AliendroidIntertitial.LoadIntertitialIron(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "FACEBOOK":
                AliendroidIntertitial.LoadIntertitialFAN(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "UNITY":
                AliendroidIntertitial.LoadIntertitialUnity(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "GOOGLE-ADS":
                AliendroidIntertitial.LoadIntertitialGoogleAds(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "ALIEN-M":
                AliendroidIntertitial.LoadIntertitialAlienMediation(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
            case "ALIEN-V":
                AliendroidIntertitial.LoadIntertitialAlienView(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL);
                break;
        }
    }

    
    public static void showInterstitial(Activity context, Boolean showNow) {
        if (showNow) {
            INTERVAL = 0;
        }
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidIntertitial.ShowIntertitialAdmob(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL,
                        HIGH_PAYING_KEYWORD1, HIGH_PAYING_KEYWORD2, HIGH_PAYING_KEYWORD3, HIGH_PAYING_KEYWORD4, HIGH_PAYING_KEYWORD5);
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                AliendroidIntertitial.ShowIntertitialApplovinDis(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidIntertitial.ShowIntertitialApplovinMax(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "IRON":
                AliendroidIntertitial.ShowIntertitialIron(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "MOPUB":
                AliendroidIntertitial.ShowIntertitialMopub(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "STARTAPP":
                AliendroidIntertitial.ShowIntertitialSartApp(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "FACEBOOK":
                AliendroidIntertitial.ShowIntertitialFAN(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "GOOGLE-ADS":
                AliendroidIntertitial.ShowIntertitialGoogleAds(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "UNITY":
                AliendroidIntertitial.ShowIntertitialUnity(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "ALIEN-V":
                AliendroidIntertitial.ShowIntertitialAlienView(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
            case "ALIEN-M":
                AliendroidIntertitial.ShowIntertitialAlienMediation(context, SELECT_BACKUP_ADS, MAIN_ADS_INTERTITIAL, BACKUP_ADS_INTERTITIAL, INTERVAL);
                break;
        }
    }

    
    public static void loadReward(Activity context) {
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidReward.LoadRewardAdmob(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidReward.LoadRewardApplovinMax(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                AliendroidReward.LoadRewardApplovinDis(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "STARTAPP":
                AliendroidReward.LoadRewardStartApp(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "IRON":
                AliendroidReward.LoadRewardIron(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "UNITY":
                AliendroidReward.LoadRewardUnity(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "GOOGLE-ADS":
                AliendroidReward.LoadRewardGoogleAds(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
        }
    }

    
    public static void showReward(Activity context) {
        // mopub fan not
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidReward.ShowRewardAdmob(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidReward.ShowRewardApplovinMax(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                AliendroidReward.ShowRewardApplovinDis(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "STARTAPP":
                AliendroidReward.ShowRewardStartApp(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "IRON":
                AliendroidReward.ShowRewardIron(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "UNITY":
                AliendroidReward.ShowRewardUnity(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
            case "GOOGLE-ADS":
                AliendroidReward.ShowRewardGoogleAds(context,SELECT_BACKUP_ADS,MAIN_ADS_REWARDS,BACKUP_ADS_REWARDS);
                break;
        }
    }

    public static void rewardListener(RewardListener rewardListener) {
        switch (SELECT_ADS) {
            case "ADMOB":
                AliendroidReward.onShowRewardsAdmob = new OnShowRewardsAdmob() {
                    @Override
                    public void onUserEarnedReward() {
                        rewardListener.onRewarded();
                    }
                };
                break;
            case "APPLOVIN-M":
            case "APPLOVIN-M-NB":
                AliendroidReward.onLoadRewardsApplovinMax = new OnLoadRewardsApplovinMax() {
                    @Override
                    public void onRewardedVideoStarted() {

                    }

                    @Override
                    public void onRewardedVideoCompleted() {
                        rewardListener.onRewarded();
                    }

                    @Override
                    public void onUserRewarded() {

                    }

                    @Override
                    public void onAdLoaded() {

                    }

                    @Override
                    public void onAdDisplayed() {

                    }

                    @Override
                    public void onAdHidden() {

                    }

                    @Override
                    public void onAdClicked() {

                    }

                    @Override
                    public void onAdLoadFailed() {
                        rewardListener.onFailed();
                    }

                    @Override
                    public void onAdDisplayFailed() {

                    }
                };
                break;
            case "APPLOVIN-D":
            case "APPLOVIN-D-NB":
                    AliendroidReward.onLoadRewardsApplovinDiscovery = new OnLoadRewardsApplovinDiscovery() {
                        @Override
                        public void adReceived() {
                            rewardListener.onRewarded();
                        }

                        @Override
                        public void failedToReceiveAd(String error) {
                            rewardListener.onFailed();
                        }
                    };
                break;
        }
    }

    public interface RewardListener {
        void onRewarded();
        void onFailed();
    }

}
