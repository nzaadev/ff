package ff.isaldev.ffskintool.Config;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.piracychecker.PiracyChecker;
import com.github.javiersantos.piracychecker.callbacks.PiracyCheckerCallback;
import com.github.javiersantos.piracychecker.enums.InstallerID;
import com.github.javiersantos.piracychecker.enums.PiracyCheckerError;
import com.github.javiersantos.piracychecker.enums.PirateApp;
import com.google.gson.Gson;

import ff.isaldev.ffskintool.Config.pojo.AdsConfigResponse;
import ff.isaldev.ffskintool.Config.pojo.AdsItem;

import timber.log.Timber;

public class NZHelper {

    public static void getRemoteConfig(Context context, String url) {
        //RequestQueue initialized
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);

        //String Request initialized
        StringRequest mStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Timber.i(response);
                parseJson(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e(error);
            }

        });

        mRequestQueue.add(mStringRequest);
    }

    public static void parseJson(String str) {
        AdsConfigResponse adsConfigResponse = new Gson().fromJson(str, AdsConfigResponse.class);
        AdsItem data =  adsConfigResponse.getAds().get(0);

        NZAdsConstant.SELECT_ADS = data.getSelectMainAds();
        NZAdsConstant.SELECT_BACKUP_ADS = data.getSelectBackupAds();
        NZAdsConstant.MAIN_ADS_INTERTITIAL = data.getMainAdsIntertitial();
        NZAdsConstant.BACKUP_ADS_INTERTITIAL = data.getBackupAdsIntertitial();
        NZAdsConstant.MAIN_ADS_BANNER = data.getMainAdsBanner();
        NZAdsConstant.BACKUP_ADS_BANNER = data.getBackupAdsBanner();
        NZAdsConstant.MAIN_ADS_NATIVES  = data.getMainAdsNatives();
        NZAdsConstant.BACKUP_ADS_NATIVES = data.getBackupAdsNatives();
        NZAdsConstant.SWITCH_OPEN_ADS = data.getSwitchOpenAds();
        NZAdsConstant.MAIN_ADS_REWARDS = data.getMainAdsRewards();
        NZAdsConstant.BACKUP_ADS_REWARDS = data.getBackupAdsRewards();
        NZAdsConstant.INITIALIZE_SDK = data.getInitializeSdk();
        NZAdsConstant.INITIALIZE_SDK_BACKUP_ADS = data.getInitializeSdkBackupAds();
        NZAdsConstant.HIGH_PAYING_KEYWORD1 = data.getHighPayingKeyword1();
        NZAdsConstant.HIGH_PAYING_KEYWORD2 = data.getHighPayingKeyword2();
        NZAdsConstant.HIGH_PAYING_KEYWORD3 = data.getHighPayingKeyword3();
        NZAdsConstant.HIGH_PAYING_KEYWORD4 = data.getHighPayingKeyword4();
        NZAdsConstant.HIGH_PAYING_KEYWORD5 = data.getHighPayingKeyword5();
        NZAdsConstant.STATUS_APP = data.getStatusApp();
        NZAdsConstant.LINK_REDIRECT = data.getLinkRedirect();
        NZAdsConstant.INTERVAL = data.getIntervalIntertitial();
        NZAdsConstant.JEDA_REWARD = data.getIntervalRewards();
        NZAdsConstant.ADMOB_OPENADS = data.getOpenAdsAdmob();
        NZAdsConstant.ONESIGNAL_APIKEY = data.getOnesignal_apikey();
    }


   public static void protectApp(Context context, PiracyCallback piracyCallback) {
        if (NZAdsConstant.PROTECT_APP) {
            new PiracyChecker(context)
                    .enableGooglePlayLicensing(NZAdsConstant.BASE_64_LICENSE_KEY)
                    .enableUnauthorizedAppsCheck()
                    .enableInstallerId(InstallerID.GOOGLE_PLAY, InstallerID.AMAZON_APP_STORE, InstallerID.GALAXY_APPS, InstallerID.HUAWEI_APP_GALLERY)
                    .saveResultToSharedPreferences("app_license", "is_licensed")
                    .callback(new PiracyCheckerCallback() {
                        @Override
                        public void allow() {
                            piracyCallback.onAllow();
                        }

                        @Override
                        public void doNotAllow(@NonNull PiracyCheckerError piracyCheckerError, @Nullable PirateApp pirateApp) {
                           piracyCallback.onDoNotAllow(piracyCheckerError);
                        }
                    })
                    .start();
        }
   }

   public static void savePremium(Context mContxt, Boolean bol){
       SharedPreferences.Editor ads =  mContxt.getSharedPreferences("ads", MODE_PRIVATE).edit();
       ads.putBoolean("premium", bol);
       ads.apply();
   }

    public static Boolean isPremium(Context mContxt){
        SharedPreferences ads =  mContxt.getSharedPreferences("ads", MODE_PRIVATE);
        return ads.getBoolean("premium", false);
    }

    public static boolean checkVPN(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network activeNet = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            activeNet = connectivityManager.getActiveNetwork();
        }
        NetworkCapabilities netCap = connectivityManager.getNetworkCapabilities(activeNet);
        return netCap.hasTransport(NetworkCapabilities.TRANSPORT_VPN);
    }


   public interface PiracyCallback {
        void onAllow();
        void onDoNotAllow(PiracyCheckerError piracyCheckerError);
   }

}
