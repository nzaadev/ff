package ff.isaldev.ffskintool.Config.pojo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdsConfigResponse {

	@SerializedName("Ads")
	private List<AdsItem> ads;

	public void setAds(List<AdsItem> ads){
		this.ads = ads;
	}

	public List<AdsItem> getAds(){
		return ads;
	}
}