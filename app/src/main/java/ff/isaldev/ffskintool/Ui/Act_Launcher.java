package ff.isaldev.ffskintool.Ui;

import static ff.isaldev.ffskintool.Config.NZAdsConstant.JSON_URL;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.aliendroid.alienads.AlienNotif;
import com.github.javiersantos.piracychecker.enums.PiracyCheckerError;

import ff.isaldev.ffskintool.Config.NZAdsConstant;
import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.Config.NZHelper;
import ff.isaldev.ffskintool.R;

public class Act_Launcher extends Activity {



    private static final long COUNTER_TIME = 3000;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.launcher);

        NZHelper.protectApp(this, new NZHelper.PiracyCallback() {
            @Override
            public void onAllow() {
                jumpToStart();
            }

            @Override
            public void onDoNotAllow(PiracyCheckerError piracyCheckerError) {
                Act_Launcher.this.runOnUiThread(() -> {
                    new AlertDialog.Builder(Act_Launcher.this)
                            .setTitle("Error")
                            .setMessage(piracyCheckerError.toString())
                            .setNegativeButton("Close", (dialogInterface, i) -> {
                                finish();
                            })
                            .setPositiveButton("Playstore", (d, __) -> {
                                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(goToMarket);
                                finish();
                            })
                            .show();
                });
            }


        });


        if(NZHelper.isPremium(this)) {
            NZAdsConstant.SELECT_ADS = "";
            NZAdsConstant.SELECT_BACKUP_ADS = "";
        } else {
            NZHelper.getRemoteConfig(this, JSON_URL);
        }

        NZAdsHelper.initialize(this);
        AlienNotif.LoadOneSignal(NZAdsConstant.ONESIGNAL_APIKEY);



        if(!NZAdsConstant.PROTECT_APP) {
            jumpToStart();
        }
    }

    private void jumpToStart() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if (isNetworkConnected()) {
                if(NZHelper.checkVPN(Act_Launcher.this)) {
                    new AlertDialog.Builder(Act_Launcher.this)
                            .setTitle("Error")
                            .setMessage("VPN Detected")
                            .setCancelable(false)
                            .setPositiveButton("Close", (dialogInterface, i) -> {
                                finish();
                            })
                            .show();

                    return;
                }

                NZAdsHelper.adsGdpr(Act_Launcher.this);
                NZAdsHelper.loadOpenAds(Act_Launcher.this);
                startMainActivity();

                finish();
            } else {
                Toast.makeText(Act_Launcher.this, "Please check your internet connections", Toast.LENGTH_SHORT).show();
            }

        }, COUNTER_TIME);
    }


    public void startMainActivity() {
        Act_Launcher.this.startActivity(new Intent(Act_Launcher.this, Act_Home.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean isNetworkConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}