package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Vehicle extends AppCompatActivity  {
    ImageView back_btn;
    RelativeLayout btn_v1;
    RelativeLayout btn_v2;
    RelativeLayout btn_v3;
    RelativeLayout btn_v4;
    RelativeLayout btn_v5;
    RelativeLayout btn_v6;
    RelativeLayout btn_v7;

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }
    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.vehicle);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.btn_v1 = (RelativeLayout) findViewById(R.id.vehicle_1);
        this.btn_v2 = (RelativeLayout) findViewById(R.id.vehicle_2);
        this.btn_v3 = (RelativeLayout) findViewById(R.id.vehicle_3);
        this.btn_v4 = (RelativeLayout) findViewById(R.id.vehicle_4);
        this.btn_v5 = (RelativeLayout) findViewById(R.id.vehicle_5);
        this.btn_v6 = (RelativeLayout) findViewById(R.id.vehicle_6);
        this.btn_v7 = (RelativeLayout) findViewById(R.id.vehicle_7);

        this.btn_v1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 1);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 2);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 3);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 4);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 5);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 6);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_v7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Vehicle.this, Act_VehilceDetail.class);
                intent.putExtra("data", 7);
                startActivity(intent);
                showInterstitial();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Vehicle.this, Act_GuideHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }


}
