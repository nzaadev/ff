package ff.isaldev.ffskintool.Ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.GetAdDetail;
import ff.isaldev.ffskintool.R;

public class Act_Privacy extends Activity {

    ImageView creation_back;
    WebView webView;


    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.privacy);

        webView = (WebView) findViewById(R.id.webV);
        webView.loadUrl("file:///android_asset/privacy.html");

        creation_back = (ImageView) findViewById(R.id.backbtn);
        creation_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Privacy.this, Act_Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}

