package ff.isaldev.ffskintool.Ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_EmoteDetail extends AppCompatActivity {

    String f149a;
    String f150b;
    ImageView back;
    String f151c;
    TextView content;
    String f152d;
    TextView data;
    String f153e;
    String f154f;
    int f156i;
    ImageView image;
    ImageView mute;
    Spanned spanned;
    ImageView speak;
    TextToSpeech textToSpeech;



    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

//        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.emote_detail);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak = (ImageView) findViewById(R.id.speak);
        this.mute = (ImageView) findViewById(R.id.mute);
        this.image = (ImageView) findViewById(R.id.image);
        int intExtra = getIntent().getIntExtra("data", 0);
        this.f156i = intExtra;
        if (intExtra == 1) {
            String string = getString(R.string.prodress1);
            this.f149a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data.setText("FF Emotes");
        } else if (intExtra == 2) {
            String string2 = getString(R.string.prodress2);
            this.f150b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data.setText("FF Emotes");
        } else if (intExtra == 3) {
            String string3 = getString(R.string.prodress3);
            this.f151c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data.setText("FF Emotes");
        } else if (intExtra == 4) {
            String string4 = getString(R.string.prodress4);
            this.f152d = string4;
            Spanned fromHtml4 = Html.fromHtml(string4);
            this.spanned = fromHtml4;
            this.content.setText(fromHtml4);
            this.data.setText("FF Emotes");
        } else if (intExtra == 5) {
            String string5 = getString(R.string.prodress5);
            this.f153e = string5;
            Spanned fromHtml5 = Html.fromHtml(string5);
            this.spanned = fromHtml5;
            this.content.setText(fromHtml5);
            this.data.setText("FF Emotes");
        } else if (intExtra == 6) {
            String string6 = getString(R.string.prodress6);
            this.f154f = string6;
            Spanned fromHtml6 = Html.fromHtml(string6);
            this.spanned = fromHtml6;
            this.content.setText(fromHtml6);
            this.data.setText("FF Emotes");
        }
        this.speak.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_EmoteDetail.this.textToSpeech = new TextToSpeech(Act_EmoteDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_EmoteDetail.this.textToSpeech.speak(Act_EmoteDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_EmoteDetail.this.textToSpeech.setLanguage(Locale.US);
                Act_EmoteDetail.this.textToSpeech.speak("Text to say aloud", 1, (HashMap) null);
                Act_EmoteDetail.this.mute.setVisibility(View.VISIBLE);
            }
        });
        this.mute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_EmoteDetail.this.mute.setVisibility(View.GONE);
                Act_EmoteDetail.this.textToSpeech.stop();
            }
        });
    }

    public void goActivate(View view) {
        showDialog();
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(Act_EmoteDetail.this);
        dialog.setContentView(R.layout.game_id_dialog);

        EditText gameId = dialog.findViewById(R.id.gameId);
        CardView claimReward = dialog.findViewById(R.id.claim);

        claimReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameId.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter Game Id !!!", Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    final ProgressDialog dialog1 = ProgressDialog.show(Act_EmoteDetail.this, "Activating Skins", "Adding Skin to FF....", true);
                    dialog1.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            dialog1.dismiss();
                            Toast.makeText(getApplicationContext(), "Skin Added.... Check FF Game After Sometime !!!", Toast.LENGTH_LONG).show();
                        }
                    }, 6000);
                }

            }
        });

        dialog.show();
    }

    public void onStop() {
        TextToSpeech textToSpeech2 = this.textToSpeech;
        if (textToSpeech2 != null) {
            textToSpeech2.stop();
            this.textToSpeech.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_EmoteDetail.this, Act_Emote.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }




}
