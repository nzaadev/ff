package ff.isaldev.ffskintool.Ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_DiamondCountDetail extends AppCompatActivity {
    int Number;
    ImageView back_btn;
    Button btn_ok;
    Dialog f117dl;
    EditText editText;
    TextView header_title;
    ImageView submit_btn;


    RelativeLayout adSpace, fl_adplaceholder;


    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

//        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.diamond_count_detail);

        loadAds();

        this.submit_btn = (ImageView) findViewById(R.id.btn_count);
        this.editText = (EditText) findViewById(R.id.img_editText);
        this.Number = getIntent().getIntExtra("Number", 0);
        TextView textView = (TextView) findViewById(R.id.txtTitle);
        this.header_title = textView;
        int i = this.Number;
        if (i == 3) {
            textView.setText(R.string.c1);
        } else if (i == 1) {
            textView.setText("Basic Diamond Calculator");
        } else {
            textView.setText("Normal Diamond Calculator");
        }
        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.submit_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (Act_DiamondCountDetail.this.Number == 3) {
                    if (Act_DiamondCountDetail.this.editText.getText().toString().isEmpty()) {
                        Toast.makeText(Act_DiamondCountDetail.this, "Please Enter Value", Toast.LENGTH_SHORT).show();
                    } else if (Act_DiamondCountDetail.this.editText.length() >= 11) {
                        Toast.makeText(Act_DiamondCountDetail.this, "Please Enter small value", Toast.LENGTH_SHORT).show();
                    } else {
                        Act_DiamondCountDetail.this.f117dl = new Dialog(Act_DiamondCountDetail.this);
                        Act_DiamondCountDetail.this.f117dl.setContentView(R.layout.dialog_calculator);
                        Act_DiamondCountDetail.this.f117dl.setCancelable(false);
                        double parseInt = (double) Integer.parseInt(Act_DiamondCountDetail.this.editText.getText().toString());
                        Double.isNaN(parseInt);
                        Double.isNaN(parseInt);
                        StringBuilder sb = new StringBuilder();
                        sb.append("If you are an advanced member you will get: ");
                        Double.isNaN(parseInt);
                        Double.isNaN(parseInt);
                        sb.append(String.valueOf(parseInt * 60.0d));
                        sb.append("ML Diamonds");
                        ((TextView) Act_DiamondCountDetail.this.f117dl.findViewById(R.id.txt_msg)).setText(sb);
                        Act_DiamondCountDetail actDiamond_CountDetail = Act_DiamondCountDetail.this;
                        actDiamond_CountDetail.btn_ok = (Button) actDiamond_CountDetail.f117dl.findViewById(R.id.rl_ok);
                        Act_DiamondCountDetail.this.btn_ok.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Act_DiamondCountDetail.this.f117dl.dismiss();
                                showInterstitial();
                            }
                        });
                        Act_DiamondCountDetail.this.f117dl.show();
                    }
                }
                if (Act_DiamondCountDetail.this.Number == 1) {
                    if (Act_DiamondCountDetail.this.editText.getText().toString().isEmpty()) {
                        Toast.makeText(Act_DiamondCountDetail.this, "Please Enter Value", Toast.LENGTH_SHORT).show();
                    } else if (Act_DiamondCountDetail.this.editText.length() >= 11) {
                        Toast.makeText(Act_DiamondCountDetail.this, "Please Enter small value", Toast.LENGTH_SHORT).show();
                    } else {
                        Act_DiamondCountDetail.this.f117dl = new Dialog(Act_DiamondCountDetail.this);
                        Act_DiamondCountDetail.this.f117dl.setContentView(R.layout.dialog_calculator);
                        Act_DiamondCountDetail.this.f117dl.setCancelable(false);
                        double parseInt2 = (double) Integer.parseInt(Act_DiamondCountDetail.this.editText.getText().toString());
                        Double.isNaN(parseInt2);
                        Double.isNaN(parseInt2);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(" Daily Basic Diamonds ");
                        ((TextView) Act_DiamondCountDetail.this.f117dl.findViewById(R.id.txt)).setText(sb2);
                        sb2.append(" Buying This Much Diamonds will cost: ");
                        Double.isNaN(parseInt2);
                        Double.isNaN(parseInt2);
                        sb2.append(String.valueOf(parseInt2 * 0.0125d));
                        sb2.append(" Dollars ");
                        ((TextView) Act_DiamondCountDetail.this.f117dl.findViewById(R.id.txt_msg)).setText(sb2);
                        Act_DiamondCountDetail actDiamond_CountDetail2 = Act_DiamondCountDetail.this;
                        actDiamond_CountDetail2.btn_ok = (Button) actDiamond_CountDetail2.f117dl.findViewById(R.id.rl_ok);
                        Act_DiamondCountDetail.this.btn_ok.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Act_DiamondCountDetail.this.f117dl.dismiss();
                                showInterstitial();

                            }
                        });
                        Act_DiamondCountDetail.this.f117dl.show();
                    }
                }
                if (Act_DiamondCountDetail.this.Number != 2) {
                    return;
                }
                if (Act_DiamondCountDetail.this.editText.getText().toString().isEmpty()) {
                    Toast.makeText(Act_DiamondCountDetail.this, "Please Enter Value", Toast.LENGTH_SHORT).show();
                } else if (Act_DiamondCountDetail.this.editText.length() >= 11) {
                    Toast.makeText(Act_DiamondCountDetail.this, "Please Enter small value", Toast.LENGTH_SHORT).show();
                } else {
                    Act_DiamondCountDetail.this.f117dl = new Dialog(Act_DiamondCountDetail.this);
                    Act_DiamondCountDetail.this.f117dl.setContentView(R.layout.dialog_calculator);
                    Act_DiamondCountDetail.this.f117dl.setCancelable(false);
                    double parseInt3 = (double) Integer.parseInt(Act_DiamondCountDetail.this.editText.getText().toString());
                    Double.isNaN(parseInt3);
                    Double.isNaN(parseInt3);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(" Daily Advanced ML Diamonds ");
                    ((TextView) Act_DiamondCountDetail.this.f117dl.findViewById(R.id.txt)).setText(sb3);
                    sb3.append(" If you play in ML Rank you will get: ");
                    Double.isNaN(parseInt3);
                    Double.isNaN(parseInt3);
                    sb3.append(String.valueOf(parseInt3 * 35.0d));
                    sb3.append(" ML Diamonds ");
                    ((TextView) Act_DiamondCountDetail.this.f117dl.findViewById(R.id.txt_msg)).setText(sb3);
                    Act_DiamondCountDetail actDiamond_CountDetail3 = Act_DiamondCountDetail.this;
                    actDiamond_CountDetail3.btn_ok = (Button) actDiamond_CountDetail3.f117dl.findViewById(R.id.rl_ok);
                    Act_DiamondCountDetail.this.btn_ok.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Act_DiamondCountDetail.this.f117dl.dismiss();
                            showInterstitial();
                        }
                    });
                    Act_DiamondCountDetail.this.f117dl.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_DiamondCountDetail.this, Act_DiamondCount.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }






}
