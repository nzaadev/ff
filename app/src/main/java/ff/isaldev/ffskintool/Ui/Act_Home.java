package ff.isaldev.ffskintool.Ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.GetAdDetail;
import ff.isaldev.ffskintool.R;

public class Act_Home extends Activity  {

    RelativeLayout adSpace, fl_adplaceholder;
    LinearLayout menu;


    private void loadAds() {
        NZAdsHelper.showOpenAds(this);
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    ImageView rate, share, more, privacy;

    void showOffer() {


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.home);
        menu = findViewById(R.id.menu);





        loadAds();
        // TODO no network
//        if (!MyApplication.isNewwokCHeck(Act_Home.this)) {
//            try {
//                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(Act_Home.this).create();
//                alertDialog.setTitle("No Internet !!!");
//                alertDialog.setMessage("Internet not available, Please enable Internet Connection and Restart App to get Free FF Skins !!!");
//                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
//                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        finish();
//                    }
//                });
//                alertDialog.show();
//            } catch (Exception e) {
//            }
//        }

        rate = (ImageView) findViewById(R.id.iv_rate);
        share = (ImageView) findViewById(R.id.iv_share);
        more = (ImageView) findViewById(R.id.iv_moreapp);
        privacy = (ImageView) findViewById(R.id.iv_privacy);

        rate.setOnClickListener(v -> {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(goToMarket);
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                    String link = "Download " + getResources().getString(R.string.app_name) + " from - https://play.google.com/store/apps/details?id="
                            + getPackageName();
                    i.putExtra(Intent.EXTRA_TEXT, link);
                    startActivity(Intent.createChooser(i, "Share Application"));
                } catch (Exception e) {
                }
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=" + GetAdDetail.MoreApps));
                startActivity(intent1);
            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Act_Home.this, Act_Privacy.class));
                finish();
            }
        });
    }

    public void goGuide(View view) {
        Intent i = new Intent(Act_Home.this, Act_Start.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
//        showInterstitial();
        finish();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(Act_Home.this, Act_Exit.class);
        Act_Home.this.startActivity(i);
    }

}