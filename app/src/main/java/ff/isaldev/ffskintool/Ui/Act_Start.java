package ff.isaldev.ffskintool.Ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Start extends Activity {

    Context context;
    Intent intent;
    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.start);
        this.context = this;

        loadAds();
    }

    public void goDiamonds(View view) {
        this.intent = new Intent(this, Act_GuideHome.class);
        startActivity(intent);
//        showInterstitial();
//        finish();
    }

    public void goSkins(View view) {
        this.intent = new Intent(this, Act_SkinHome.class);
        startActivity(intent);
//        showInterstitial();
//        finish();
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Start.this, Act_Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }
}
