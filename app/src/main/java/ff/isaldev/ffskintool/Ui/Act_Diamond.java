package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Diamond extends AppCompatActivity {
    ImageView back_btn;
    TextView header_txt;
    Spanned spanned;
    RelativeLayout t1_btn;
    RelativeLayout t2_btn;
    RelativeLayout t3_btn;


    RelativeLayout adSpace, fl_adplaceholder;


    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.diamond);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.header_txt = (TextView) findViewById(R.id.txt);
        this.t1_btn = (RelativeLayout) findViewById(R.id.img_playgame);
        this.t2_btn = (RelativeLayout) findViewById(R.id.img_entergame);
        this.t3_btn = (RelativeLayout) findViewById(R.id.img_freebox);
        Spanned fromHtml = Html.fromHtml(getString(R.string.tips2));
        this.spanned = fromHtml;
        this.header_txt.setText(fromHtml);
        this.t1_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Diamond.this, Act_DiamondDetail.class);
                intent.putExtra("data", 1);
                startActivity(intent);
                showInterstitial();
                // show inters
            }
        });
        this.t2_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Diamond.this, Act_DiamondDetail.class);
                intent.putExtra("data", 2);
                startActivity(intent);
                // show inters
                showInterstitial();
            }
        });
        this.t3_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Diamond.this, Act_DiamondDetail.class);
                intent.putExtra("data", 3);
                startActivity(intent);
                // show inters
                showInterstitial();
            }
        });
    }

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }


}
