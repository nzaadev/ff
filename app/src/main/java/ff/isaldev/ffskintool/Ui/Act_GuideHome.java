package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_GuideHome extends AppCompatActivity  {


    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    Intent intent;

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        //loadAds();

    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.guidehome);

        loadAds();
    }

    public void goBack(View view) {
        onBackPressed();
    }

    public void goTips(View view) {
        this.intent = new Intent(this, Act_Tips.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goCharacter(View view) {
        this.intent = new Intent(this, Act_Characters.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goDiamonds(View view) {
        this.intent = new Intent(this, Act_Diamond.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }


    public void goDiamondsSpin(View view) {
        this.intent = new Intent(this, Act_DiamondSpinner.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goDiamondsCount(View view) {
        this.intent = new Intent(this, Act_DiamondCount.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goVehicles(View view) {
        this.intent = new Intent(this, Act_Vehicle.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goWeapons(View view) {
        this.intent = new Intent(this, Act_Weapons.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }



}
