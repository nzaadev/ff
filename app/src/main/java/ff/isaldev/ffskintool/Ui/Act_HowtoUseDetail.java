package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_HowtoUseDetail extends AppCompatActivity  {

    String f149a;
    ImageView back;
    TextView content;
    TextView data;
    ImageView image;
    ImageView mute;
    Spanned spanned;
    ImageView speak;
    TextToSpeech textToSpeech;



    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.howtouse_detail);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak = (ImageView) findViewById(R.id.speak);
        this.mute = (ImageView) findViewById(R.id.mute);
        this.image = (ImageView) findViewById(R.id.image);

        String string = getString(R.string.howtouse);
        this.f149a = string;
        Spanned fromHtml = Html.fromHtml(string);
        this.spanned = fromHtml;
        this.content.setText(fromHtml);
        this.data.setText("How to Use ?");

        this.speak.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_HowtoUseDetail.this.textToSpeech = new TextToSpeech(Act_HowtoUseDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_HowtoUseDetail.this.textToSpeech.speak(Act_HowtoUseDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_HowtoUseDetail.this.textToSpeech.setLanguage(Locale.US);
                Act_HowtoUseDetail.this.textToSpeech.speak("Text to say aloud", 1, (HashMap) null);
                Act_HowtoUseDetail.this.mute.setVisibility(View.VISIBLE);
            }
        });
        this.mute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_HowtoUseDetail.this.mute.setVisibility(View.GONE);
                Act_HowtoUseDetail.this.textToSpeech.stop();
            }
        });
    }

    public void goGet(View view) {
        onBackPressed();
    }

    public void onStop() {
        TextToSpeech textToSpeech2 = this.textToSpeech;
        if (textToSpeech2 != null) {
            textToSpeech2.stop();
            this.textToSpeech.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_HowtoUseDetail.this, Act_SkinHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }


}
