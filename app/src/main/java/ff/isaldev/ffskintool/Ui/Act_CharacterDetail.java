package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_CharacterDetail extends AppCompatActivity {

    String f101a;
    String f102a1;
    String f103b;
    String f104b1;
    ImageView back_btn;
    String f105c;
    String f106c1;
    TextView content;
    String f107d;
    String f108d1;
    TextView data1;
    String f109e;
    String f110e1;
    String f111f;
    String f112f1;
    String f113g;
    String f114g1;
    String f115h1;
    int f116i;
    ImageView img_btn;
    ImageView mute_btn;
    Spanned spanned;
    ImageView speak_btn;
    TextToSpeech tts;


    RelativeLayout adSpace;
    RelativeLayout fl_adplaceholder;


    // inters native banner

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();
        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.character_detail);

        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });
        this.data1 = (TextView) findViewById(R.id.data1);
        this.content = (TextView) findViewById(R.id.content);
        this.speak_btn = (ImageView) findViewById(R.id.speak);
        this.mute_btn = (ImageView) findViewById(R.id.mute);
        this.img_btn = (ImageView) findViewById(R.id.image);
        int intExtra = getIntent().getIntExtra("data", 0);
        this.f116i = intExtra;
        if (intExtra == 1) {
            String string = getString(R.string.per1);
            this.f101a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data1.setText("Hayato");
        } else if (intExtra == 2) {
            String string2 = getString(R.string.per2);
            this.f103b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data1.setText("Moco");
        } else if (intExtra == 3) {
            String string3 = getString(R.string.per3);
            this.f105c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data1.setText("Wukong");
        } else if (intExtra == 4) {
            String string4 = getString(R.string.per4);
            this.f107d = string4;
            Spanned fromHtml4 = Html.fromHtml(string4);
            this.spanned = fromHtml4;
            this.content.setText(fromHtml4);
            this.data1.setText("Antonio");
        } else if (intExtra == 5) {
            String string5 = getString(R.string.per5);
            this.f109e = string5;
            Spanned fromHtml5 = Html.fromHtml(string5);
            this.spanned = fromHtml5;
            this.content.setText(fromHtml5);
            this.data1.setText("Andrew");
        } else if (intExtra == 6) {
            String string6 = getString(R.string.per6);
            this.f111f = string6;
            Spanned fromHtml6 = Html.fromHtml(string6);
            this.spanned = fromHtml6;
            this.content.setText(fromHtml6);
            this.data1.setText("Kelly");
        } else if (intExtra == 7) {
            String string7 = getString(R.string.per7);
            this.f113g = string7;
            Spanned fromHtml7 = Html.fromHtml(string7);
            this.spanned = fromHtml7;
            this.content.setText(fromHtml7);
            this.data1.setText("Olivia");
        } else if (intExtra == 8) {
            String string8 = getString(R.string.per8);
            this.f102a1 = string8;
            Spanned fromHtml8 = Html.fromHtml(string8);
            this.spanned = fromHtml8;
            this.content.setText(fromHtml8);
            this.data1.setText("Ford");
        } else if (intExtra == 9) {
            String string9 = getString(R.string.per9);
            this.f104b1 = string9;
            Spanned fromHtml9 = Html.fromHtml(string9);
            this.spanned = fromHtml9;
            this.content.setText(fromHtml9);
            this.data1.setText("Nikita");
        } else if (intExtra == 10) {
            String string10 = getString(R.string.per10);
            this.f106c1 = string10;
            Spanned fromHtml10 = Html.fromHtml(string10);
            this.spanned = fromHtml10;
            this.content.setText(fromHtml10);
            this.data1.setText("Misha");
        } else if (intExtra == 11) {
            String string11 = getString(R.string.per11);
            this.f108d1 = string11;
            Spanned fromHtml11 = Html.fromHtml(string11);
            this.spanned = fromHtml11;
            this.content.setText(fromHtml11);
            this.data1.setText("Maxim");
        } else if (intExtra == 12) {
            String string12 = getString(R.string.per12);
            this.f110e1 = string12;
            Spanned fromHtml12 = Html.fromHtml(string12);
            this.spanned = fromHtml12;
            this.content.setText(fromHtml12);
            this.data1.setText("Kla");
        } else if (intExtra == 13) {
            String string13 = getString(R.string.per13);
            this.f112f1 = string13;
            Spanned fromHtml13 = Html.fromHtml(string13);
            this.spanned = fromHtml13;
            this.content.setText(fromHtml13);
            this.data1.setText("Paloma");
        } else if (intExtra == 14) {
            String string14 = getString(R.string.per14);
            this.f114g1 = string14;
            Spanned fromHtml14 = Html.fromHtml(string14);
            this.spanned = fromHtml14;
            this.content.setText(fromHtml14);
            this.data1.setText("Miguel");
        } else if (intExtra == 15) {
            String string15 = getString(R.string.per15);
            this.f115h1 = string15;
            Spanned fromHtml15 = Html.fromHtml(string15);
            this.spanned = fromHtml15;
            this.content.setText(fromHtml15);
            this.data1.setText("Carroline");
        }
        this.speak_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_CharacterDetail.this.tts = new TextToSpeech(Act_CharacterDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_CharacterDetail.this.tts.speak(Act_CharacterDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_CharacterDetail.this.tts.setLanguage(Locale.US);
                Act_CharacterDetail.this.tts.speak("Text to say aloud", 1, (HashMap) null);
                Act_CharacterDetail.this.mute_btn.setVisibility(View.VISIBLE);
            }
        });
        this.mute_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_CharacterDetail.this.mute_btn.setVisibility(View.GONE);
                Act_CharacterDetail.this.tts.stop();
            }
        });
    }

    public void onStop() {
        TextToSpeech textToSpeech = this.tts;
        if (textToSpeech != null) {
            textToSpeech.stop();
            this.tts.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_CharacterDetail.this, Act_Characters.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        showInterstitial();
        finish();
    }



    private void loadAds() {
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }


}
