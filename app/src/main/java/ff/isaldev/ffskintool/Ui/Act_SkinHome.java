package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_SkinHome extends AppCompatActivity  {

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    Intent intent;

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();

    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.skinhome);

        loadAds();
    }

    public void goBack(View view) {
        onBackPressed();
    }

    public void goSkin1(View view) {
        this.intent = new Intent(this, Act_ProDress.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goSkin2(View view) {
        this.intent = new Intent(this, Act_Emote.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goSkin3(View view) {
        this.intent = new Intent(this, Act_Guns.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goSkin4(View view) {
        this.intent = new Intent(this, Act_Trending.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goSkin5(View view) {
        this.intent = new Intent(this, Act_ReferFriend.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }

    public void goSkin6(View view) {
        this.intent = new Intent(this, Act_HowtoUseDetail.class);
        startActivity(intent);
        showInterstitial();
        finish();
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_SkinHome.this, Act_Start.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }
}
