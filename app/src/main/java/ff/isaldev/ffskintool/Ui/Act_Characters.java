package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;


import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Characters extends AppCompatActivity {
    ImageView back_btn;
    RelativeLayout chr_1;
    RelativeLayout chr_10;
    RelativeLayout chr_11;
    RelativeLayout chr_12;
    RelativeLayout chr_13;
    RelativeLayout chr_14;
    RelativeLayout chr_15;
    RelativeLayout chr_2;
    RelativeLayout chr_3;
    RelativeLayout chr_4;
    RelativeLayout chr_5;
    RelativeLayout chr_6;
    RelativeLayout chr_7;
    RelativeLayout chr_8;
    RelativeLayout chr_9;


    RelativeLayout adSpace;
    RelativeLayout fl_adplaceholder;


    @Override
    protected void onResume() {
        super.onResume();
        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.characters);

        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                onBackPressed();
            }
        });

        this.chr_1 = (RelativeLayout) findViewById(R.id.charac_1);
        this.chr_2 = (RelativeLayout) findViewById(R.id.charac_2);
        this.chr_3 = (RelativeLayout) findViewById(R.id.charac_3);
        this.chr_4 = (RelativeLayout) findViewById(R.id.charac_4);
        this.chr_5 = (RelativeLayout) findViewById(R.id.charac_5);
        this.chr_6 = (RelativeLayout) findViewById(R.id.charac_6);
        this.chr_7 = (RelativeLayout) findViewById(R.id.charac_7);
        this.chr_8 = (RelativeLayout) findViewById(R.id.charac_8);
        this.chr_9 = (RelativeLayout) findViewById(R.id.charac_9);
        this.chr_10 = (RelativeLayout) findViewById(R.id.charac_10);
        this.chr_11 = (RelativeLayout) findViewById(R.id.charac_11);
        this.chr_12 = (RelativeLayout) findViewById(R.id.charac_12);
        this.chr_13 = (RelativeLayout) findViewById(R.id.charac_13);
        this.chr_14 = (RelativeLayout) findViewById(R.id.charac_14);
        this.chr_15 = (RelativeLayout) findViewById(R.id.charac_15);

        this.chr_1.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 1);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.chr_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 2);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.chr_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 3);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.chr_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 4);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.chr_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 5);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.chr_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 6);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 7);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 8);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 9);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 10);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_11.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 11);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_12.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 12);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_13.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 13);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_14.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 14);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.chr_15.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Characters.this, Act_CharacterDetail.class);
                intent.putExtra("data", 15);
                startActivity(intent);
                showInterstitial();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Characters.this, Act_GuideHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    private void loadAds() {
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

}
