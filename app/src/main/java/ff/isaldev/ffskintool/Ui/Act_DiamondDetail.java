package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_DiamondDetail extends AppCompatActivity {
    String f118a;
    String f119b;
    ImageView back_btn;
    String f120c;
    TextView content;
    TextView data;
    int f125i;
    ImageView mute_btn;
    Spanned spanned;
    ImageView speak_btn;
    TextToSpeech tts;


    RelativeLayout adSpace, fl_adplaceholder;


    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.diamond_detail);

        loadAds();

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak_btn = (ImageView) findViewById(R.id.speak);
        this.mute_btn = (ImageView) findViewById(R.id.mute);

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });
        int intExtra = getIntent().getIntExtra("data", 0);
        this.f125i = intExtra;
        if (intExtra == 1) {
            String string = getString(R.string.tips3);
            this.f118a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data.setText(R.string.tips31);
        } else if (intExtra == 2) {
            String string2 = getString(R.string.tips4);
            this.f119b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data.setText(R.string.tips41);
        } else if (intExtra == 3) {
            String string3 = getString(R.string.tips5);
            this.f120c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data.setText(R.string.tips51);
        }
        this.speak_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_DiamondDetail.this.tts = new TextToSpeech(Act_DiamondDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_DiamondDetail.this.tts.speak(Act_DiamondDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_DiamondDetail.this.tts.setLanguage(Locale.US);
                Act_DiamondDetail.this.tts.speak("Text to say aloud", 1, (HashMap) null);
                Act_DiamondDetail.this.mute_btn.setVisibility(View.VISIBLE);
            }
        });
        this.mute_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_DiamondDetail.this.mute_btn.setVisibility(View.GONE);
                Act_DiamondDetail.this.tts.stop();
            }
        });
    }

    public void onStop() {
        TextToSpeech textToSpeech = this.tts;
        if (textToSpeech != null) {
            textToSpeech.stop();
            this.tts.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_DiamondDetail.this, Act_Diamond.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }



}
