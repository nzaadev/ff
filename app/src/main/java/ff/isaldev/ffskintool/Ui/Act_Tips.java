package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Tips extends AppCompatActivity  {
    ImageView btn_back;
    RelativeLayout btn_tt1;
    RelativeLayout btn_tt2;
    RelativeLayout btn_tt3;
    RelativeLayout btn_tt4;
    RelativeLayout btn_tt5;
    RelativeLayout btn_tt6;
    RelativeLayout btn_tt7;

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }
    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.tips);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.btn_back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.btn_tt1 = (RelativeLayout) findViewById(R.id.tips_1);
        this.btn_tt2 = (RelativeLayout) findViewById(R.id.tips_2);
        this.btn_tt3 = (RelativeLayout) findViewById(R.id.tips_3);
        this.btn_tt4 = (RelativeLayout) findViewById(R.id.tips_4);
        this.btn_tt5 = (RelativeLayout) findViewById(R.id.tips_5);
        this.btn_tt6 = (RelativeLayout) findViewById(R.id.tips_6);
        this.btn_tt7 = (RelativeLayout) findViewById(R.id.tips_7);

        this.btn_tt1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 1);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 2);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 3);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 4);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 5);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 6);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_tt7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Tips.this, Act_TipsDetail.class);
                intent.putExtra("data", 7);
                startActivity(intent);
                showInterstitial();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Tips.this, Act_GuideHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }



}
