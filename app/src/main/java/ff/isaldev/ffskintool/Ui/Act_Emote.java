package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_Emote extends AppCompatActivity  {
    ImageView btn_back;
    RelativeLayout btn_w1;
    RelativeLayout btn_w2;
    RelativeLayout btn_w3;
    RelativeLayout btn_w4;
    RelativeLayout btn_w5;
    RelativeLayout btn_w6;



    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

//        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.emote);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.btn_back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.btn_w1 = (RelativeLayout) findViewById(R.id.weapon_1);
        this.btn_w2 = (RelativeLayout) findViewById(R.id.weapon_2);
        this.btn_w3 = (RelativeLayout) findViewById(R.id.weapon_3);
        this.btn_w4 = (RelativeLayout) findViewById(R.id.weapon_4);
        this.btn_w5 = (RelativeLayout) findViewById(R.id.weapon_5);
        this.btn_w6 = (RelativeLayout) findViewById(R.id.weapon_6);

        this.btn_w1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 1);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_w2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 2);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_w3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 3);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_w4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 4);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.btn_w5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 5);
                startActivity(intent);
                showInterstitial();
            }
        });
        this.btn_w6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_Emote.this, Act_EmoteDetail.class);
                intent.putExtra("data", 6);
                startActivity(intent);
                showInterstitial();
            }
        });
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_Emote.this, Act_SkinHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }




}
