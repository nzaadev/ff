package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_VehilceDetail extends AppCompatActivity  {

    String f141a;

    String f142b;
    ImageView back_btn;
    String f143c;
    TextView content;
    String f144d;
    TextView data;
    String f145e;
    String f146f;
    String f147g;
    int f148i;
    ImageView iimg_btn;
    ImageView mute_btn;
    Spanned spanned;
    ImageView speak_btn;
    TextToSpeech tts;

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.vehicle_detail);

       loadAds();

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak_btn = (ImageView) findViewById(R.id.speak);
        this.mute_btn = (ImageView) findViewById(R.id.mute);
        this.iimg_btn = (ImageView) findViewById(R.id.image);
        this.f148i = getIntent().getIntExtra("data", 0);

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        int i = this.f148i;
        if (i == 1) {
            String string = getString(R.string.per1);
            this.f141a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data.setText(R.string.vv_1);
        } else if (i == 2) {
            String string2 = getString(R.string.per2);
            this.f142b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data.setText(R.string.vv_2);
        } else if (i == 3) {
            String string3 = getString(R.string.per3);
            this.f143c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data.setText(R.string.vv_3);
        } else if (i == 4) {
            String string4 = getString(R.string.per4);
            this.f144d = string4;
            Spanned fromHtml4 = Html.fromHtml(string4);
            this.spanned = fromHtml4;
            this.content.setText(fromHtml4);
            this.data.setText(R.string.vv_4);
        } else if (i == 5) {
            String string5 = getString(R.string.per5);
            this.f145e = string5;
            Spanned fromHtml5 = Html.fromHtml(string5);
            this.spanned = fromHtml5;
            this.content.setText(fromHtml5);
            this.data.setText(R.string.vv_5);
        } else if (i == 6) {
            String string6 = getString(R.string.per6);
            this.f146f = string6;
            Spanned fromHtml6 = Html.fromHtml(string6);
            this.spanned = fromHtml6;
            this.content.setText(fromHtml6);
            this.data.setText(R.string.vv_6);
        } else if (i == 7) {
            String string7 = getString(R.string.per7);
            this.f147g = string7;
            Spanned fromHtml7 = Html.fromHtml(string7);
            this.spanned = fromHtml7;
            this.content.setText(fromHtml7);
            this.data.setText(R.string.vv_7);
        }
        this.speak_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_VehilceDetail.this.tts = new TextToSpeech(Act_VehilceDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_VehilceDetail.this.tts.speak(Act_VehilceDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_VehilceDetail.this.tts.setLanguage(Locale.US);
                Act_VehilceDetail.this.tts.speak("Text to say aloud", 1, (HashMap) null);
                Act_VehilceDetail.this.mute_btn.setVisibility(View.VISIBLE);
            }
        });
        this.mute_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_VehilceDetail.this.mute_btn.setVisibility(View.GONE);
                Act_VehilceDetail.this.tts.stop();
            }
        });
    }

    public void onStop() {
        TextToSpeech textToSpeech = this.tts;
        if (textToSpeech != null) {
            textToSpeech.stop();
            this.tts.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_VehilceDetail.this, Act_Vehicle.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }


}
