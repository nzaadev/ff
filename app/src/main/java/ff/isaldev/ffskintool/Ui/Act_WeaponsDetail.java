package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_WeaponsDetail extends AppCompatActivity  {

    String f149a;
    String f150b;
    ImageView back;
    String f151c;
    TextView content;
    String f152d;
    TextView data;
    String f153e;
    String f154f;
    String f155g;
    int f156i;
    ImageView image;
    ImageView mute;
    Spanned spanned;
    ImageView speak;
    TextToSpeech textToSpeech;


    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.weapons_detail);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak = (ImageView) findViewById(R.id.speak);
        this.mute = (ImageView) findViewById(R.id.mute);
        this.image = (ImageView) findViewById(R.id.image);
        int intExtra = getIntent().getIntExtra("data", 0);
        this.f156i = intExtra;
        if (intExtra == 1) {
            String string = getString(R.string.wea1);
            this.f149a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data.setText("AN94");
        } else if (intExtra == 2) {
            String string2 = getString(R.string.wea2);
            this.f150b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data.setText("M4A1");
        } else if (intExtra == 3) {
            String string3 = getString(R.string.wea3);
            this.f151c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data.setText("M14");
        } else if (intExtra == 4) {
            String string4 = getString(R.string.wea4);
            this.f152d = string4;
            Spanned fromHtml4 = Html.fromHtml(string4);
            this.spanned = fromHtml4;
            this.content.setText(fromHtml4);
            this.data.setText("AK");
        } else if (intExtra == 5) {
            String string5 = getString(R.string.wea5);
            this.f153e = string5;
            Spanned fromHtml5 = Html.fromHtml(string5);
            this.spanned = fromHtml5;
            this.content.setText(fromHtml5);
            this.data.setText("SCAR");
        } else if (intExtra == 6) {
            String string6 = getString(R.string.wea6);
            this.f154f = string6;
            Spanned fromHtml6 = Html.fromHtml(string6);
            this.spanned = fromHtml6;
            this.content.setText(fromHtml6);
            this.data.setText("GROZA");
        } else if (intExtra == 7) {
            String string7 = getString(R.string.wea7);
            this.f155g = string7;
            Spanned fromHtml7 = Html.fromHtml(string7);
            this.spanned = fromHtml7;
            this.content.setText(fromHtml7);
            this.data.setText("FAMAS");
        }
        this.speak.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_WeaponsDetail.this.textToSpeech = new TextToSpeech(Act_WeaponsDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_WeaponsDetail.this.textToSpeech.speak(Act_WeaponsDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_WeaponsDetail.this.textToSpeech.setLanguage(Locale.US);
                Act_WeaponsDetail.this.textToSpeech.speak("Text to say aloud", 1, (HashMap) null);
                Act_WeaponsDetail.this.mute.setVisibility(View.VISIBLE);
            }
        });
        this.mute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_WeaponsDetail.this.mute.setVisibility(View.GONE);
                Act_WeaponsDetail.this.textToSpeech.stop();
            }
        });
    }

    public void onStop() {
        TextToSpeech textToSpeech2 = this.textToSpeech;
        if (textToSpeech2 != null) {
            textToSpeech2.stop();
            this.textToSpeech.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_WeaponsDetail.this, Act_Weapons.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }



}
