package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_DiamondCount extends AppCompatActivity {
    ImageView back_btn;
    RelativeLayout count_one;
    RelativeLayout count_three;
    RelativeLayout count_two;
    TextView header_txt;


    RelativeLayout adSpace, fl_adplaceholder;


    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

//        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.diamond_count);

        loadAds();

        this.count_one = (RelativeLayout) findViewById(R.id.basic_img);
        this.count_two = (RelativeLayout) findViewById(R.id.normal_img);
        this.count_three = (RelativeLayout) findViewById(R.id.advance_img);
        this.header_txt = (TextView) findViewById(R.id.header_title);

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                onBackPressed();
            }
        });

        this.count_one.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                Intent intent = new Intent(Act_DiamondCount.this, Act_DiamondCountDetail.class);
                intent.putExtra("Number", 1);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.count_two.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_DiamondCount.this, Act_DiamondCountDetail.class);
                intent.putExtra("Number", 2);
                startActivity(intent);
                showInterstitial();
            }
        });

        this.count_three.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Act_DiamondCount.this, Act_DiamondCountDetail.class);
                intent.putExtra("Number", 3);
                startActivity(intent);
                showInterstitial();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_DiamondCount.this, Act_GuideHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

}
