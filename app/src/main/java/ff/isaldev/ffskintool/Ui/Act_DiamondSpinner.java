package ff.isaldev.ffskintool.Ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.GetAdDetail;
import ff.isaldev.ffskintool.R;

public class Act_DiamondSpinner extends AppCompatActivity  {

    private static final String[] f126K = {"32", "15", "19", "4", "21", ExifInterface.GPS_MEASUREMENT_2D, "25", "17", "34", "6", "27", "13", "36", "11", "30", "8", "23", "10", "5", "24", "16", "33", "1", "20", "14", "31", "9", "22", "18", "29", "7", "28", "12", "35", ExifInterface.GPS_MEASUREMENT_3D, "26", "0"};

    private static final Random f127L = new Random();

    private int f128E = 0;

    private int f129F = 0;

    RoundCornerProgressBar RProgress;
    ArrayList<String> arr1;
    ArrayList<String> arr2;
    ArrayList<String> arr3;
    ArrayList<String> arr4;
    ImageView btn_back;
    ImageView btn_img;
    TextView btn_result;
    SharedPreferences.Editor editor;
    SharedPreferences shared_pre;
    TextView txt_spins;
    ImageView wheel;
    TextView youwin, waitText;
    boolean waitNextSpin = false;
    boolean firtSpin = true;



    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

//        loadAds();
    }

    public void onCreate(Bundle bundle) {
        ImageView imageView;
        Drawable drawable;
        super.onCreate(bundle);
        setContentView((int) R.layout.diamond_spinner);

        loadAds();
        waitText = findViewById(R.id.wait);


        RoundCornerProgressBar roundCornerProgressBar = (RoundCornerProgressBar) findViewById(R.id.RoundCornerProgressBar);
        this.RProgress = roundCornerProgressBar;
        roundCornerProgressBar.setMax(30000.0f);
        ImageView imageView2 = (ImageView) findViewById(R.id.img_back);
        this.btn_back = imageView2;
        imageView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayList<String> arrayList = new ArrayList<>();
        this.arr1 = arrayList;
        arrayList.addAll(Arrays.asList(new String[]{"32", "15", "19", "4", "21"}));
        ArrayList<String> arrayList2 = new ArrayList<>();
        this.arr2 = arrayList2;
        arrayList2.addAll(Arrays.asList(new String[]{"22", "18", "29", "7", "28", "12"}));
        ArrayList<String> arrayList3 = new ArrayList<>();
        this.arr3 = arrayList3;
        arrayList3.addAll(Arrays.asList(new String[]{"33", "16", "24", "24", "5"}));
        ArrayList<String> arrayList4 = new ArrayList<>();
        this.arr4 = arrayList4;
        arrayList4.addAll(Arrays.asList(new String[]{"63", "13", "27", "6", "36", "11"}));
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.txt_spins = (TextView) findViewById(R.id.txt_spins);
        this.youwin = (TextView) findViewById(R.id.youwin);
        ImageView imageView3 = (ImageView) findViewById(R.id.Spinlay);
        this.btn_img = imageView3;
        imageView3.setOnClickListener(new View.OnClickListener() {
            // TODO
            public void onClick(View view) {
//                NZAdsHelper.showReward(Act_DiamondSpinner.this);
                if(GetAdDetail.adCount == 3){
                    GetAdDetail.adCount = 0;
//                    showInterstitial();
                } else {
                    GetAdDetail.adCount++;
                    Act_DiamondSpinner.this.spin();
                }


            }
        });
        this.btn_result = (TextView) findViewById(R.id.txt_result);
        this.wheel = (ImageView) findViewById(R.id.wheelbtn);
        this.txt_spins = (TextView) findViewById(R.id.txt_spins);
        SharedPreferences sharedPreferences2 = getSharedPreferences("mycoins", 0);
        this.shared_pre = sharedPreferences2;
        this.editor = sharedPreferences2.edit();
        this.RProgress.setProgress((float) this.shared_pre.getInt("coins", 0));
        this.btn_result.setText(String.valueOf(this.shared_pre.getInt("coins", 0)));
        this.txt_spins.setText(String.valueOf(this.shared_pre.getInt("spins", 0)));
        if (this.shared_pre.getInt("coins", 0) == 0) {
            this.editor.putInt("spins", 100);
            this.editor.apply();
            this.txt_spins.setText(String.valueOf(this.shared_pre.getInt("spins", 0)));
        } else {
            this.shared_pre.getInt("spins", 0);
        }
        if (this.shared_pre.getInt("spins", 0) == 0) {
            this.btn_img.setEnabled(false);
            if (Build.VERSION.SDK_INT >= 21) {
                imageView = this.btn_img;
                drawable = getResources().getDrawable(R.drawable.spin_img, getApplicationContext().getTheme());
            } else {
                imageView = this.btn_img;
                drawable = getResources().getDrawable(R.drawable.spin_img);
            }
            imageView.setImageDrawable(drawable);
        }
        this.RProgress.setProgress((float) this.shared_pre.getInt("coins", 0));

//        spin();
    }

    public class C0871a implements DialogInterface.OnClickListener {
        C0871a() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            Act_DiamondSpinner.this.editor.putInt("coins", 0);
            Act_DiamondSpinner.this.editor.putInt("spins", 100);
            Act_DiamondSpinner.this.editor.apply();
            Act_DiamondSpinner.this.finish();
        }
    }

    public class C0872h implements Animation.AnimationListener {
        public void onAnimationRepeat(Animation animation) {
        }

        public C0872h() {
        }

        public void onAnimationEnd(Animation animation) {

            ImageView imageView = null;
            if (Act_DiamondSpinner.this.shared_pre.getInt("spins", 0) == 0) {
                Act_DiamondSpinner.this.btn_img.setEnabled(false);
                if (Build.VERSION.SDK_INT < 21) {
                    imageView.setImageDrawable(Act_DiamondSpinner.this.getResources().getDrawable(R.drawable.spin_img));
                    Act_DiamondSpinner.this.mo12033S();
                    Act_DiamondSpinner.this.RProgress.setProgress((float) Act_DiamondSpinner.this.shared_pre.getInt("coins", 0));
                }
            } else {
                Act_DiamondSpinner.this.btn_img.setEnabled(true);
                if (Build.VERSION.SDK_INT < 21) {
                    imageView.setImageDrawable(Act_DiamondSpinner.this.getResources().getDrawable(R.drawable.spin_img));
                    Act_DiamondSpinner.this.mo12033S();
                    Act_DiamondSpinner.this.RProgress.setProgress((float) Act_DiamondSpinner.this.shared_pre.getInt("coins", 0));
                }
            }
            Act_DiamondSpinner.this.btn_img.setImageResource(R.drawable.spin_img);
            Act_DiamondSpinner.this.mo12033S();
            Act_DiamondSpinner.this.RProgress.setProgress((float) Act_DiamondSpinner.this.shared_pre.getInt("coins", 0));

//            NZAdsHelper.showReward(Act_DiamondSpinner.this);
            showInterstitial();
        }



        public void onAnimationStart(Animation animation) {
            ImageView imageView;
            Drawable drawable;
            Act_DiamondSpinner.this.btn_img.setEnabled(false);
            Act_DiamondSpinner.this.youwin.setText("");
            if (Build.VERSION.SDK_INT >= 21) {
                imageView = Act_DiamondSpinner.this.btn_img;
                drawable = Act_DiamondSpinner.this.getResources().getDrawable(R.drawable.spin_img, Act_DiamondSpinner.this.getApplicationContext().getTheme());
            } else {
                imageView = Act_DiamondSpinner.this.btn_img;
                drawable = Act_DiamondSpinner.this.getResources().getDrawable(R.drawable.spin_img);
            }
            imageView.setImageDrawable(drawable);
        }
    }

    private String m19N(int i) {
        int i2 = 0;
        String str = null;
        do {
            int i3 = i2 * 2;
            float f = ((float) (i3 + 3)) * 4.864865f;
            float f2 = (float) i;
            if (f2 >= ((float) (i3 + 1)) * 4.864865f && f2 < f) {
                str = f126K[i2];
            }
            i2++;
            if (str != null || i2 >= f126K.length) {
                return str;
            }
            int i32 = i2 * 2;
            float f3 = ((float) (i32 + 3)) * 4.864865f;
            float f22 = (float) i;
            str = f126K[i2];
            i2++;
            break;
        } while (i2 >= f126K.length);
        return str;
    }

    public void Arr_hand(ArrayList<String> arrayList, int i) {
        String str;
        TextView textView;
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            try {
                if (m19N(360 - (this.f128E % 360)).equals(arrayList.get(i2))) {
                    Log.e("SpinCount", m19N(360 - (this.f128E % 360)).equals(arrayList.get(i2)) + "");
                    if (i == 250) {
                        this.editor.putInt("coins", this.shared_pre.getInt("coins", 0) + ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
                        this.editor.apply();
                        this.btn_result.setText(String.valueOf(this.shared_pre.getInt("coins", 0)));
                        textView = this.youwin;
                        str = "+250 Diamonds";
                    } else if (i == 450) {
                        this.editor.putInt("coins", this.shared_pre.getInt("coins", 0) + 450);
                        this.editor.apply();
                        this.btn_result.setText(String.valueOf(this.shared_pre.getInt("coins", 0)));
                        textView = this.youwin;
                        str = "+450 Diamonds";
                    } else if (i == 750) {
                        this.editor.putInt("coins", this.shared_pre.getInt("coins", 0) + 250);
                        this.editor.apply();
                        this.btn_result.setText(String.valueOf(this.shared_pre.getInt("coins", 0)));
                        textView = this.youwin;
                        str = "+250 Diamonds";
                    } else if (i != 800) {
                        textView = this.youwin;
                        str = "try again";
                    } else {
                        this.editor.putInt("coins", this.shared_pre.getInt("coins", 0) + 800);
                        this.editor.apply();
                        this.btn_result.setText(String.valueOf(this.shared_pre.getInt("coins", 0)));
                        textView = this.youwin;
                        str = "+800 Diamonds";
                    }
                    textView.setText(str);
                }
            } catch (RuntimeException unused) {
            }
        }
        this.RProgress.setProgress((float) this.shared_pre.getInt("coins", 0));
        if (this.shared_pre.getInt("coins", 0) > 30000) {
            this.editor.putInt("coins", 0);
            this.editor.putInt("spins", 100);
            this.editor.apply();

            Intent intent = new Intent(Act_DiamondSpinner.this, Act_Congratulation.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            showInterstitial();
            finish();
        }
    }

    public void mo12033S() {
        Log.e("SpinArr", arr1.size()+"");
        Log.e("SpinArr", arr2.size()+"");
        Log.e("SpinArr", arr3.size()+"");
        Log.e("SpinArr", arr4.size()+"");
        Arr_hand(this.arr1, 750);
        Arr_hand(this.arr2, 250);
        Arr_hand(this.arr4, 450);
        Arr_hand(this.arr3, 800);
    }

    public void onBackPressed() {


        Intent intent = new Intent(Act_DiamondSpinner.this, Act_GuideHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();

    }



    public void spin() {

        if(firtSpin) {
            spins();
            firtSpin = false;
            return;
        }
        // can spin == false && firsspin == false
        if(waitNextSpin) {
            Log.e("TAG", "spin: WAIT" );
        } else {
            Log.e("TAG", "spin: SPIN!!" );
            spins();
            // tunggu 5 detik utk spin selanjutnya supaya tidak spam spin
            CountDownTimer count = new CountDownTimer(5000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    waitNextSpin = true;
                    Act_DiamondSpinner.this.runOnUiThread(() -> {
                        waitText.setText(String.format("Please wait %s seconds", millisUntilFinished/1000));
                    });
                }

                @Override
                public void onFinish() {
                    // spin boleh
                    waitText.setText("");
                    waitNextSpin = false;
                }
            };

            count.start();

        }

    }

    public void spins() {
        ImageView imageView;
        Drawable drawable;


        if (this.shared_pre.getInt("spins", 0) <= 1) {
            this.btn_img.setEnabled(false);
            if (Build.VERSION.SDK_INT >= 21) {
                imageView = this.btn_img;
                drawable = getResources().getDrawable(R.drawable.spin_img, getApplicationContext().getTheme());
            } else {
                imageView = this.btn_img;
                drawable = getResources().getDrawable(R.drawable.spin_img);
            }
            imageView.setImageDrawable(drawable);
        }
        this.editor.putInt("spins", this.shared_pre.getInt("spins", 0) - 1);
        this.editor.apply();
        this.txt_spins.setText(String.valueOf(this.shared_pre.getInt("spins", 0)));
        this.f129F = this.f128E % 360;
        this.f128E = f127L.nextInt(360) + 720;
        RotateAnimation rotateAnimation = new RotateAnimation((float) this.f129F, (float) this.f128E, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(1500);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setAnimationListener(new C0872h());
        wheel.startAnimation(rotateAnimation);

    }

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadReward(Act_DiamondSpinner.this);
//        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
        NZAdsHelper.showInterstitial(this, false);
    }

}
