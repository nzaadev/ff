package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_TipsDetail extends AppCompatActivity  {

    String f133a;
    String f134b;
    ImageView back_btn;
    String f135c;
    TextView content;
    String f136d;
    TextView data;
    String f137e;
    String f138f;
    String f139g;
    int f140i;
    ImageView mute_btn;
    Spanned spanned;
    ImageView speak_btn;
    TextToSpeech tts;

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.tips_detail);

        loadAds();

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak_btn = (ImageView) findViewById(R.id.speak);
        this.mute_btn = (ImageView) findViewById(R.id.mute);
        this.f140i = getIntent().getIntExtra("data", 0);

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back_btn = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        int i = this.f140i;
        if (i == 1) {
            String string = getString(R.string.trik1);
            this.f133a = string;
            Spanned fromHtml = Html.fromHtml(string);
            this.spanned = fromHtml;
            this.content.setText(fromHtml);
            this.data.setText("Choose The Correct Landing Site");
        } else if (i == 2) {
            String string2 = getString(R.string.trik2);
            this.f134b = string2;
            Spanned fromHtml2 = Html.fromHtml(string2);
            this.spanned = fromHtml2;
            this.content.setText(fromHtml2);
            this.data.setText("Get Equipment");
        } else if (i == 3) {
            String string3 = getString(R.string.trik3);
            this.f135c = string3;
            Spanned fromHtml3 = Html.fromHtml(string3);
            this.spanned = fromHtml3;
            this.content.setText(fromHtml3);
            this.data.setText("Explore military sites");
        } else if (i == 4) {
            String string4 = getString(R.string.trik4);
            this.f136d = string4;
            Spanned fromHtml4 = Html.fromHtml(string4);
            this.spanned = fromHtml4;
            this.content.setText(fromHtml4);
            this.data.setText("Please Pay Attention to the safe area");
        } else if (i == 5) {
            String string5 = getString(R.string.trik5);
            this.f137e = string5;
            Spanned fromHtml5 = Html.fromHtml(string5);
            this.spanned = fromHtml5;
            this.content.setText(fromHtml5);
            this.data.setText("Look at the mini map");
        } else if (i == 6) {
            String string6 = getString(R.string.trik6);
            this.f138f = string6;
            Spanned fromHtml6 = Html.fromHtml(string6);
            this.spanned = fromHtml6;
            this.content.setText(fromHtml6);
            this.data.setText("Take cover");
        } else if (i == 7) {
            String string7 = getString(R.string.trik7);
            this.f139g = string7;
            Spanned fromHtml7 = Html.fromHtml(string7);
            this.spanned = fromHtml7;
            this.content.setText(fromHtml7);
            this.data.setText("Use the vehicle carefully");
        }
        this.speak_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_TipsDetail.this.tts = new TextToSpeech(Act_TipsDetail.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_TipsDetail.this.tts.speak(Act_TipsDetail.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_TipsDetail.this.tts.setLanguage(Locale.US);
                Act_TipsDetail.this.tts.speak("Text to say aloud", 1, (HashMap) null);
                Act_TipsDetail.this.mute_btn.setVisibility(View.VISIBLE);
            }
        });
        this.mute_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_TipsDetail.this.mute_btn.setVisibility(View.GONE);
                Act_TipsDetail.this.speak_btn.setVisibility(View.VISIBLE);
                Act_TipsDetail.this.tts.stop();
            }
        });
    }

    public void onStop() {
        TextToSpeech textToSpeech = this.tts;
        if (textToSpeech != null) {
            textToSpeech.stop();
            this.tts.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_TipsDetail.this, Act_Tips.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }



}
