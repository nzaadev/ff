package ff.isaldev.ffskintool.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Locale;

import ff.isaldev.ffskintool.Config.NZAdsHelper;
import ff.isaldev.ffskintool.R;

public class Act_ReferFriend extends AppCompatActivity  {

    String f149a;
    ImageView back;
    TextView content;
    TextView data;
    ImageView image;
    ImageView mute;
    Spanned spanned;
    ImageView speak;
    TextToSpeech textToSpeech;

    RelativeLayout adSpace, fl_adplaceholder;

    private void loadAds() {
        adSpace =  findViewById(R.id.last);
        fl_adplaceholder = findViewById(R.id.fl_adplaceholder);
        NZAdsHelper.loadInterstitial(this);
        NZAdsHelper.natives(this, fl_adplaceholder);
        NZAdsHelper.banner(this, adSpace);
    }

    private void showInterstitial() {
         NZAdsHelper.showInterstitial(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adSpace.removeAllViews();

        loadAds();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.referfriend_detail);

        loadAds();

        ImageView imageView = (ImageView) findViewById(R.id.img_back);
        this.back = imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        this.data = (TextView) findViewById(R.id.data);
        this.content = (TextView) findViewById(R.id.content);
        this.speak = (ImageView) findViewById(R.id.speak);
        this.mute = (ImageView) findViewById(R.id.mute);
        this.image = (ImageView) findViewById(R.id.image);

        String string = getString(R.string.referearn);
        this.f149a = string;
        Spanned fromHtml = Html.fromHtml(string);
        this.spanned = fromHtml;
        this.content.setText(fromHtml);
        this.data.setText("How to Use ?");

        this.speak.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_ReferFriend.this.textToSpeech = new TextToSpeech(Act_ReferFriend.this, new TextToSpeech.OnInitListener() {
                    public void onInit(int i) {
                        Act_ReferFriend.this.textToSpeech.speak(Act_ReferFriend.this.content.getText().toString(), 0, (HashMap) null);
                    }
                });
                Act_ReferFriend.this.textToSpeech.setLanguage(Locale.US);
                Act_ReferFriend.this.textToSpeech.speak("Text to say aloud", 1, (HashMap) null);
                Act_ReferFriend.this.mute.setVisibility(View.VISIBLE);
            }
        });
        this.mute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Act_ReferFriend.this.mute.setVisibility(View.GONE);
                Act_ReferFriend.this.textToSpeech.stop();
            }
        });
    }

    public void goRefer(View view) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String link = "Download " + getResources().getString(R.string.app_name) + " from - https://play.google.com/store/apps/details?id="
                    + getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, link);
            startActivity(Intent.createChooser(i, "Share Application"));
        } catch (Exception e) {
        }
    }

    public void onStop() {
        TextToSpeech textToSpeech2 = this.textToSpeech;
        if (textToSpeech2 != null) {
            textToSpeech2.stop();
            this.textToSpeech.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Act_ReferFriend.this, Act_SkinHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        showInterstitial();
        finish();
    }

}
